﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    public GameObject ParticlePrefab;
    public GameObject RepulsivePrefab;
    public GameObject ViolentPrefab;
    public GameObject SonarPrefab;
    public GameObject ContaminatedPrefab;
    public GameObject SolidPrefab;
    //public GameObject PatternVPrefab;
    //public GameObject PatternSPrefab;
    //public GameObject PatternVRPrefab;
    public Vector2 center;
    public Vector2 size;
    
    public int nViolentMax;
    public int nRepulsiveMax;
    public int nParticleMax;
    public int nSonarMax;
    public int nContaminatedMax;
    public int nSolidMax;
    //public int nPatternSMax;
    //public int nPatternVMax;
    //public int nPatternVRMax;
    private int ParticleRespawn;
    private int nParticleToSpawn;
    private int nParticle;
    public int nParticleNeeded;
    public float ContaminatedSetting;
    public float PatternSetting;
    public float ViolentSetting;

    private Scene ThisScene;
    public string scene;

    void Start()
    {    
        SpawnParticle();
        ViolentParticle();
        ContaminatedParticle();
        RepulsiveParticle();
        SonarParticle();
        SolidParticle();
        //PatternViolent();
        //PatternVR();
        //PatternSolid();
        ThisScene = SceneManager.GetActiveScene();
        scene = ThisScene.name;

        if(scene == "Level0")
        {
            ParticleBase();
        }
    }

    void Update()
    {
       if(scene != "Level0")
       {
        nParticle = GameObject.FindGameObjectsWithTag("Particle").Length;
        ParticleRespawn = nParticleMax - nParticle;
            
        if(nParticle < nParticleNeeded)
        {
            nParticleToSpawn = ParticleRespawn;
            SpawnParticle();
        }
       }
    }

    public void SolidParticle()
    {
        for(int i = 0; i < nSolidMax; i++)
        {
        Vector2 pos0 = new Vector2(Random.Range(-size.x/2, size.x/2), Random.Range(-size.y/2, size.y/2));
        Instantiate(SolidPrefab, pos0, Quaternion.identity);
        }
    }

    public void ViolentParticle()
    {
        for(int i = 0; i < nViolentMax; i++)
        {
        Vector2 pos1 = new Vector2(Random.Range(-size.x/ViolentSetting, size.x/ViolentSetting), Random.Range(-size.y/ViolentSetting, size.y/ViolentSetting));
        Instantiate(ViolentPrefab, pos1, Quaternion.identity);
        }   
    }

    public void ContaminatedParticle()
    {
        for(int i = 0; i < nContaminatedMax; i++)
        {
        Vector2 pos2 = new Vector2(Random.Range(-size.x/ContaminatedSetting, size.x/ContaminatedSetting), Random.Range(-size.y/ContaminatedSetting, size.y/ContaminatedSetting));
        Instantiate(ContaminatedPrefab, pos2, Quaternion.identity);
        }
    }

    public void RepulsiveParticle()
    {
        for(int i = 0; i < nRepulsiveMax; i++)
        {
        Vector2 pos3 = new Vector2(Random.Range(-size.x/2, size.x/2), Random.Range(-size.y/2, size.y/2));
        Instantiate(RepulsivePrefab, pos3, Quaternion.identity);
        }
    }

    public void SonarParticle()
    {
        for(int i = 0; i < nSonarMax; i++)
        {
        Vector2 pos4 = new Vector2(Random.Range(-size.x/2, size.x/2), Random.Range(-size.y/2, size.y/2));
        Instantiate(SonarPrefab, pos4, Quaternion.identity);
        }        
    }

    public void SpawnParticle()
    {  
       for(int i = 0; i < nParticleToSpawn; i++)
       {
       Vector2 pos = new Vector2(Random.Range(-size.x/2, size.x/2), Random.Range(-size.y/2, size.y/2));
       Instantiate(ParticlePrefab, pos, Quaternion.identity);
       }
    }
    
    public void ParticleBase()
    {
       for(int i = 0; i < nParticleMax; i++)
       {
       Vector2 pos5 = new Vector2(Random.Range(-size.x/2, size.x/2), Random.Range(-size.y/2, size.y/2));
       Instantiate(ParticlePrefab, pos5, Quaternion.identity);
       }       
    }

    /*public void PatternViolent()
    {
       for(int i = 0; i < nPatternVMax; i++)
       {
       Vector2 pos5 = new Vector2(Random.Range(-size.x/PatternSetting, size.x/PatternSetting), Random.Range(-size.y/PatternSetting, size.y/PatternSetting));
       Instantiate(PatternVPrefab, pos5, Quaternion.identity);
       }       
    }

    public void PatternVR()
    {
       for(int i = 0; i < nPatternVRMax; i++)
       {
       Vector2 pos5 = new Vector2(Random.Range(-size.x/PatternSetting, size.x/PatternSetting), Random.Range(-size.y/PatternSetting, size.y/PatternSetting));
       Instantiate(PatternVRPrefab, pos5, Quaternion.identity);
       }       
    }

    public void PatternSolid()
    {
       for(int i = 0; i < nPatternSMax; i++)
       {
       Vector2 pos5 = new Vector2(Random.Range(-size.x/PatternSetting, size.x/PatternSetting), Random.Range(-size.y/PatternSetting, size.y/PatternSetting));
       Instantiate(PatternSPrefab, pos5, Quaternion.identity);
       }       
    }*/

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1,0,0.5f);
        Gizmos.DrawCube(center,size);
    }
}
