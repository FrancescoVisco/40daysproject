﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayer : MonoBehaviour
{
    public bool TriggerIn;
    public bool TriggerActivated;
    public GameObject Video;
    public Animator Fade;
    public AudioSource VideoActivationSound;

    void Start()
    {
        
    }

    void Update()
    {
        if(TriggerIn == true)
        {
           Video.SetActive(true);
           TriggerActivated = true;
        }
        else
        {
           //Video.SetActive(false);
        }    
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            TriggerIn = true;
            Fade.SetBool("Fade", true);
            VideoActivationSound.Play();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            TriggerIn = false;
        }
    }
}
