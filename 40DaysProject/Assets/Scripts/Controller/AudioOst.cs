﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioOst : MonoBehaviour
{
    
    public GameObject OST0;
    public GameObject OST1;
    public GameObject OST2;
    public GameObject OST3;
    public GameObject OST4;
    
    private Scene ThisScene;
    private string scene;

    void Start()
    {
               
    }

    void Update()
    {
        DontDestroyOnLoad(this.gameObject);
        ThisScene = SceneManager.GetActiveScene();
        scene = ThisScene.name;
    
        if(scene == "Level0" || scene == "0_MainMenu")
        {
            OST0.SetActive(true);
            OST1.SetActive(false);
            OST2.SetActive(false);
            OST3.SetActive(false);
            OST4.SetActive(false);
        }

        if(scene == "Level1")
        {
            OST0.SetActive(false);
            OST1.SetActive(true);
            OST2.SetActive(false);
            OST3.SetActive(false);
            OST4.SetActive(false);
        }

        if(scene == "Level2")
        {
            OST0.SetActive(false);
            OST1.SetActive(false);
            OST2.SetActive(true);
            OST3.SetActive(false);
            OST4.SetActive(false);
        }


        if(scene == "Level3")
        {
            OST0.SetActive(false);
            OST1.SetActive(false);
            OST2.SetActive(false);
            OST3.SetActive(true);
            OST4.SetActive(false);
        }


        if(scene == "Level4")
        {
            OST0.SetActive(false);
            OST1.SetActive(false);
            OST2.SetActive(false);
            OST3.SetActive(false);
            OST4.SetActive(true);
        }
    }
}
