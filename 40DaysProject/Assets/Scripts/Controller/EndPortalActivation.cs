﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPortalActivation : MonoBehaviour
{
    public bool Video0;
    public bool Video1;
    public bool Video2;
    public bool Video3;
    public GameObject EndPortal;

    void Start()
    {
        Video0 = GameObject.Find("Video0").GetComponent<VideoPlayer>().TriggerActivated;
        Video1 = GameObject.Find("Video1").GetComponent<VideoPlayer>().TriggerActivated;
        Video2 = GameObject.Find("Video2").GetComponent<VideoPlayer>().TriggerActivated;
        Video3 = GameObject.Find("Video3").GetComponent<VideoPlayer>().TriggerActivated;
    }

    void Update()
    {
        Video0 = GameObject.Find("Video0").GetComponent<VideoPlayer>().TriggerActivated;
        Video1 = GameObject.Find("Video1").GetComponent<VideoPlayer>().TriggerActivated;
        Video2 = GameObject.Find("Video2").GetComponent<VideoPlayer>().TriggerActivated;
        Video3 = GameObject.Find("Video3").GetComponent<VideoPlayer>().TriggerActivated;

        if(Video0 == true && Video1 == true && Video2 == true && Video3 == true)
        {
           EndPortal.SetActive(true);
        }
    }
}
