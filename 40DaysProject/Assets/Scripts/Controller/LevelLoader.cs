﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float transitionTime;
    public float transitionMin;
    public float transitionMax;
    public float transitionEnd;
    public bool Fade = false;

    public TextMeshProUGUI Text;
    public TextMeshProUGUI CitText;
    public string[] Phrases;
    public string[] Cit;
    public GameObject TextToShow;
    public Animator TextShowing;
    public Animator CitShowing;

    private Scene ThisScene;
    private string scene;

    void Update()
    {
       ThisScene = SceneManager.GetActiveScene();
       scene = ThisScene.name;

       if(Fade == true)
       {
          LoadNextLevel();
          transition.SetBool("Fade", true);
       }

       if(Input.GetKeyDown(KeyCode.L))
       {
         Fade = true;
         
         if(scene == "Level4End")
         {
            Fade = true;
         }
       }

       if(Input.GetKeyDown(KeyCode.N))
       {
         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);

         if(scene == "Level4End")
         {
            SceneManager.LoadScene(0);
         }
       }

       if(scene == "0_MainMenu" || scene == "Level0" || scene == "Level1")
       {
          transitionTime = transitionMin;
       }
       
       if(scene == "Level2" || scene == "Level3" || scene == "Level4End")
       {
          transitionTime = transitionMax;
       }

       if(scene == "Level4")
       {
          transitionTime = transitionEnd;
       }


      //Gestione e assegnazione frasi 
       if(scene == "0_MainMenu")
       {
          Text.text = Phrases[0];
          CitText.text = Cit[0];
       }

       if(scene == "Level0")
       {
          Text.text = Phrases[1];
          CitText.text = Cit[1];
       }

       if(scene == "Level1")
       {
          Text.text = Phrases[2];
          CitText.text = Cit[2];
       }

       if(scene == "Level2")
       {
          Text.text = Phrases[3];
          CitText.text = Cit[3];
       }

       if(scene == "Level3")
       {
          Text.text = Phrases[4];
          CitText.text = Cit[4];
       }

       if(scene == "Level4End")
       {
          Text.text = Phrases[5];
          CitText.text = Cit[5];
       }
    }

    public void LoadNextLevel()
    {
      if(scene == "Level4End")
      {
         StartCoroutine(LoadLevel(0)); 
      }
      else
      {
         StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex+1)); 
      }       
    }

    IEnumerator LoadLevel(int LevelIndex)
    {  
       yield return new WaitForSeconds(2.5f);
       TextToShow.SetActive(true);
       yield return new WaitForSeconds(transitionTime - 3f);
       TextShowing.SetBool("Fade", true);
       CitShowing.SetBool("Fade", true);
       yield return new WaitForSeconds(4.0f);
       SceneManager.LoadScene(LevelIndex);
    }
}
