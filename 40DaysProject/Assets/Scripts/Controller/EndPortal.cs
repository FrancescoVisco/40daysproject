﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPortal : MonoBehaviour
{

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
           GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
           GameObject.Find("Molecule").GetComponent<PlayerController>().Control = false;
           GameObject.Find("Molecule").GetComponent<PlayerController>().EndPortal = true;
        }
    }
}
