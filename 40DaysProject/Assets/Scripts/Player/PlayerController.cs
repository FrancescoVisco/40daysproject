﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	[Header("Basic settings")]
    public float Speed;
	public int particles;
	public Rigidbody2D rb;
	private float RepulsiveForce = 35f;

	[Header("Sonar settings")]
	public float SonarTime;
	public GameObject Sonar;
	public bool SonarActivated;

    [Header("Decontamination settings")]
	public bool Control;
	private bool MaxLoad;
	public bool Rotation;
	public float OrbitSpeed;
	public float OrbitSpeedMax;
	public GameObject Orbit;
	public GameObject TrailLine;
	public float RotationDelay;
	public float multiplier;
	public GameObject WindDecontamination;
	public GameObject DecontaminationSound;

	[Header("EndPortal settings")]
    public bool EndPortal;
	public GameObject TargetEnd;
	public float TargetSpeed;

    [Header("Particles settings")]
	public ParticleSystem ps;
	public GameObject Particle;
	ParticleSystem.EmissionModule emission;
	public float EmissionRateMin;
	public float EmissionRateMax;

	[Header("Sounds settings")]
	public AudioSource BasicCollision;
	public AudioSource SonarSound;
	public AudioSource ParticlesCollision;
	public GameObject ParticlesMaxLoad;
	public AudioSource[] ParticleSounds;

	[Header("Scene settings")]	
	private Scene ThisScene;
    private string scene;

	void Start()
	{
      SonarActivated = false;
      emission = ps.emission;
	  Control = true;
	}

	void Update()
	{

	Orbit = GameObject.FindWithTag("Contaminated");
	TargetEnd = GameObject.Find("TargetEnd");
	ThisScene = SceneManager.GetActiveScene();
    scene = ThisScene.name;

    // Movement key
    	if(Control == true)
		{
       		if((Input.GetKey(KeyCode.RightArrow)) || (Input.GetKey(KeyCode.D)))
	   		{
		   		rb.AddForce(new Vector2 (Speed,0f));
	   		}
	   		if((Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A)))
	   		{
			   	rb.AddForce(new Vector2 (-Speed,0f));
	   		}
	   		if((Input.GetKey(KeyCode.UpArrow)) || (Input.GetKey(KeyCode.W)))
	   		{
			   	rb.AddForce(new Vector2 (0f,Speed));
	   		}
	   		if((Input.GetKey(KeyCode.DownArrow)) || (Input.GetKey(KeyCode.S)))
	   		{
		 	  	rb.AddForce(new Vector2 (0f,-Speed));
	   		}
		}
		else if(Control == false && EndPortal == false)
		{
			if(Rotation == true)
			{
				OrbitSpeed += Time.deltaTime * multiplier;
				transform.RotateAround(Orbit.transform.position, Vector3.forward, OrbitSpeed * Time.deltaTime);
				TrailLine.SetActive(true);
				Particle.SetActive(false);
				WindDecontamination.SetActive(true);
				DecontaminationSound.SetActive(true);
			}

			if(OrbitSpeed > OrbitSpeedMax)
			{
				OrbitSpeed = OrbitSpeedMax;
				GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
			}
		}
		else if(Control == false && EndPortal == true)
		{
			transform.position = Vector3.MoveTowards(transform.position, TargetEnd.transform.position, TargetSpeed * Time.deltaTime);
		}

       //Particle controller
	   if(particles<0)
	   {
		   particles = 0;
	   }

	   // Particle Laod

	   if(scene != "Level4End")
	   {
    	   if(particles < 3)
	   		{
		   		Speed = 30f;
	   		}

	   		if(particles > 3 && particles < 7)
	   		{
		   		Speed = 25f;
	   		}

	   		if(particles > 7 && particles <10)
	   		{
		   		Speed = 20f;
	  		}

	   		if(particles == 10)
	   		{
		   		Speed = 15f;
		  		MaxLoad = true;
		   		ParticlesMaxLoad.SetActive(true);
	  		}
	   		else
	   		{
           		ParticlesMaxLoad.SetActive(false);
		   		MaxLoad = false;
	   		}
	   }
	   else
	   {
            particles = 10;
			Speed = 15f;
			MaxLoad = true;
	   }


	   if(Rotation == true)
	   {
		   ParticlesMaxLoad.SetActive(false);
	   }

       // Emission rate Controller
	   if((Input.GetAxis("Horizontal") == 0) && (Input.GetAxis("Vertical") == 0))
	   {
         emission.rateOverTime = EmissionRateMin;
	   }
	   else
	   {
		 emission.rateOverTime = EmissionRateMax;
	   }

	   //Sonar Activation
	   if(SonarActivated == true)
	   {
        StartCoroutine("SonarEffect");		
	   }	   
	   else
	   {
        StopCoroutine("SonarEffect");
	   }
	}


	IEnumerator SonarEffect()
	{
		yield return new WaitForSeconds(SonarTime);
		SonarSound.Stop();
		SonarActivated = false;
	}

    //Trail Activation
	IEnumerator TrailEffect()
	{
		Control = false;
		yield return new WaitForSeconds(RotationDelay);
		Rotation = true;
	}
    
	//Molecule collision
	void OnCollisionEnter2D(Collision2D other)
    {

      if (other.gameObject.CompareTag("Particle"))
      {
		if(particles == 10)
		{   	
           other.rigidbody.AddForce(-other.contacts[0].normal * RepulsiveForce, ForceMode2D.Impulse);
		   if(Rotation == false)
		   {
              BasicCollision.Play();
		   }
		}		
      }

   	  if (other.gameObject.CompareTag("Violent"))
      {
		  particles -= 5;
      }

      if (other.gameObject.CompareTag("Contaminated"))
      {
		if(MaxLoad == true)
		{
			StartCoroutine("TrailEffect");
		}
	  }

	  if (other.gameObject.CompareTag("Wall"))
      {
		BasicCollision.Play();
	  }

	  if (other.gameObject.CompareTag("Solid"))
      {
		BasicCollision.Play();
	  }
    }

	//Particle collision
   	void OnTriggerEnter2D(Collider2D col)
    {
      if (col.gameObject.CompareTag("Particle"))
      {
		if(particles<10 && Rotation == false)
		{
           Destroy(col.gameObject);
		   particles += 1;
		   ParticlesCollision.Play();
		}
        
		//Particles PickUp Sounds

		if(Rotation == true)
		{
		   Destroy(col.gameObject);
		   BasicCollision.Play();	   
		}
	  }

	  if (col.gameObject.CompareTag("Sonar"))
      {
		  if(SonarActivated == false)
		  {		  
			Destroy(col.gameObject);
			SonarSound.Play();
		    SonarActivated = true;  
		  }
      }
    }
}