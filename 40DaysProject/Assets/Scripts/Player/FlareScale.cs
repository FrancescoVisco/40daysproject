﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareScale : MonoBehaviour
{
    public float x;
    public float y;
    public float multiplier;
    public float Delay;
    public int particles;
    public Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        //Scale Flare
        particles = GameObject.Find("Molecule").GetComponent<PlayerController>().particles;
        y = x;
        transform.localScale = new Vector2(x, y);

        if(particles == 10 && GameObject.Find("Molecule").GetComponent<PlayerController>().Rotation == false)  
        {
           anim.enabled = true;
        }
        else
        {
           anim.enabled = false;
        }

        if(x<0)
        {
           x = 0;
        }

        if(GameObject.Find("Molecule").GetComponent<PlayerController>().Rotation == true)
        {
           StartCoroutine("Wait");
        }
        else
        {
            switch(particles)    
            {
               case 0:
                x = 0f;
                break;

               case 1:
                x = 1f;
                break;

               case 2:
                x = 2f;
                break;  

               case 3:
                x = 2.5f;
                break;

               case 4:
                x = 3f;
                break;    
                 
               case 5:
                x = 3.5f;
                break;

               case 6:
                x = 4f;
                break;    

               case 7:
                x = 4.5f;
                break;  

               case 8:
                x = 5f;
                break;

               case 9:
                x = 5.5f;
                break;    
                 
               case 10:
                x = 6f;
                break;                         
            }
        }
    }

    IEnumerator Wait()
    {
      yield return new WaitForSeconds(Delay);
      x -= Time.deltaTime;
      anim.enabled = false;
    }
}