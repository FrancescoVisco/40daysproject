﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecontaminationDash : MonoBehaviour
{

    public bool Contamination;
    public float DecontaminationDashSpeed;
    public GameObject Contaminated;
    
    
    void Start()
    {
      Contamination = GameObject.FindWithTag("Contaminated").GetComponent<ContaminatedMolecule>().Contamination;
      Contaminated = GameObject.FindWithTag("Contaminated");
    }


    void Update()
    {
      Contamination = GameObject.FindWithTag("Contaminated").GetComponent<ContaminatedMolecule>().Contamination;
      Contaminated = GameObject.FindWithTag("Contaminated");

      if(Contamination == false)
      {
        transform.position = Vector2.MoveTowards(transform.position, Contaminated.transform.position, -DecontaminationDashSpeed * Time.deltaTime);
      }
    }
}
