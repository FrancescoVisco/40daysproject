﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeViolent : MonoBehaviour 
{
    public AudioSource ViolentCollision;

    void Start()
    {

    }

    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D other)
    {
       if(other.gameObject.CompareTag("Particle"))
       {
          Destroy(other.gameObject);
       }

       if(other.gameObject.CompareTag("Player"))
       {
          ViolentCollision.Play();
       }
    }
}