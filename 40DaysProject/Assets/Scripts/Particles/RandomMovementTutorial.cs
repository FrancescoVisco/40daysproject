﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public class RandomMovementTutorial : MonoBehaviour 
 {
   private float latestDirectionChangeTime;
   public float directionChangeTime;
   public float characterVelocity;
   private Vector2 movementDirection;
   private Vector2 movementPerSecond;
   public bool TutorialScene = true;
   public GameObject Target;
   public float TargetSpeed;
 
   void Start(){
     latestDirectionChangeTime = 1f;
     calcuateNewMovementVector();
   }
 
   void calcuateNewMovementVector(){
    //create a random direction vector with the magnitude of 1, later multiply it with the velocity of the enemy
     movementDirection = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
     movementPerSecond = movementDirection * characterVelocity;
   }
 
   void Update(){
    if(TutorialScene == false)
    {
     if (Time.time - latestDirectionChangeTime > directionChangeTime)
     {
         latestDirectionChangeTime = Time.time;
         calcuateNewMovementVector();
     }

     //move enemy: 
     transform.position = new Vector2(transform.position.x + (movementPerSecond.x * Time.deltaTime), 
     transform.position.y + (movementPerSecond.y * Time.deltaTime));
    }
    else
    {
      transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, TargetSpeed * Time.deltaTime);
    }
   }


   void OnCollisionEnter2D(Collision2D other)
   {
       if(other.gameObject.CompareTag("Target"))
       {
          TutorialScene = false; 
       }
   }
}

