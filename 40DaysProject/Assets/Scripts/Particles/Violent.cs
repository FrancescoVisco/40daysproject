﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Violent : MonoBehaviour 
{
    [Header("Random Movement")]
    public float directionChangeTime;
    public float characterVelocity;
    private float latestDirectionChangeTime;
    private Vector2 movementDirection;
    private Vector2 movementPerSecond;
       
    [Header("Dash settings")]
    public bool OnDash;
    public bool Hit;
    public bool IsViolent;
    public bool Contamination;
    public GameObject Contaminated;
    
    public bool CoroutineOn;
    public float DashSpeed;
    public float DashTime;
    public float DecontaminationDashSpeed;
    
    public float Delay;
    public float distance;
    public float RealDistance;
    public GameObject target;
    
    public Scene ThisScene;
    public string scene;

    [Header("Sounds settings")]
    public AudioSource ViolentCollision;

    void Start()
    {
      latestDirectionChangeTime = 1f;
      calcuateNewMovementVector();
      target = GameObject.FindGameObjectWithTag("Player");
      Contaminated = GameObject.FindGameObjectWithTag("Contaminated");
      Contamination = GameObject.FindGameObjectWithTag("Contaminated").GetComponent<ContaminatedMolecule>().Contamination;
      //emission = ps.emission;
    }

    void calcuateNewMovementVector()
    {
      movementDirection = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
      movementPerSecond = movementDirection * characterVelocity;
    }

    void Update()
    {
     target = GameObject.FindGameObjectWithTag("Player");
     Contaminated = GameObject.FindGameObjectWithTag("Contaminated");
     Contamination = GameObject.FindGameObjectWithTag("Contaminated").GetComponent<ContaminatedMolecule>().Contamination;
     RealDistance = Vector2.Distance(transform.position, target.transform.position);
     ThisScene = SceneManager.GetActiveScene();
     scene = ThisScene.name;

     //Movement, Dash and Shake
      if(OnDash == false)
      {
        if (Time.time - latestDirectionChangeTime > directionChangeTime)
        {
           latestDirectionChangeTime = Time.time;
           calcuateNewMovementVector();
        }
        transform.position = new Vector2(transform.position.x + (movementPerSecond.x * Time.deltaTime), 
        transform.position.y + (movementPerSecond.y * Time.deltaTime));
      }
      else if(OnDash == true && CoroutineOn == false && Contamination == true)
      {
        transform.position = Vector2.MoveTowards(transform.position, target.transform.position, DashSpeed * Time.deltaTime);
      }
      
      if(Contamination == false)
      {
        transform.position = Vector2.MoveTowards(transform.position, Contaminated.transform.position, -DecontaminationDashSpeed * Time.deltaTime);
      }


     //Dash Controller
     if((Vector2.Distance(transform.position, target.transform.position) < distance) && (IsViolent == true) && (CoroutineOn == false) && (Hit == false))
     {
        OnDash = true;
        StartCoroutine("Dash");
     }
     else
     {
        OnDash = false;
     }

     //IsViolent Controller
     if(scene == "Level3")
     {
         IsViolent = true;
     }
     else
     {
         IsViolent = false;
     }
    }

    public IEnumerator Dash()
    {  
        yield return new WaitForSeconds(Delay);
        CoroutineOn = true;
        //GameObject.FindGameObjectWithTag("Violent").GetComponent<Violent>().Hit = false;
        Hit = false;
        yield return new WaitForSeconds(DashTime);
        CoroutineOn = false;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
       if(other.gameObject.CompareTag("Particle"))
       {
          Destroy(other.gameObject);
       }

       if(other.gameObject.CompareTag("Player"))
       {
          //GameObject.FindGameObjectWithTag("Violent").GetComponent<Violent>().Hit = true;
          Hit = true;
          ViolentCollision.Play();
       }

       if(other.gameObject.CompareTag("Solid"))
       {
          //GameObject.FindGameObjectWithTag("Violent").GetComponent<Violent>().Hit = true;
          Hit = true;
       }

       if(other.gameObject.CompareTag("Contaminated"))
       {
          //GameObject.FindGameObjectWithTag("Violent").GetComponent<Violent>().Hit = true;
          Hit = true;
       }

       if(other.gameObject.CompareTag("Repulsive"))
       {
          //GameObject.FindGameObjectWithTag("Violent").GetComponent<Violent>().Hit = true;
          Hit = true;
       }
    }
}