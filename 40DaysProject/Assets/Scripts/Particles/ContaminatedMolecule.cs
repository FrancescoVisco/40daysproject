﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContaminatedMolecule : MonoBehaviour
{
    public Animator animator;
    public int particles;
    public int particleneeded;
    public bool Contamination = true;
    public GameObject ps;
    //public GameObject SonarEffect;

    void Start()
    {

    }

    void Update()
    {
        particles = GameObject.Find("Molecule").GetComponent<PlayerController>().particles;
        /*
        if(GameObject.Find("TargetIndicator").GetComponent<WindowSonarArrow>().SonarIsActivated == false)
        {
          SonarEffect.SetActive(false);
        }
        else
        {
          SonarEffect.SetActive(true);
        }
        */
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
          if(particles == particleneeded && Contamination == true)
          {
            //GameObject.Find("Molecule").GetComponent<PlayerController>().particles = 0;
            animator.SetBool("Decontamination", true);
            Contamination = false;
            ps.SetActive(true);
          }
        } 
    }
}
