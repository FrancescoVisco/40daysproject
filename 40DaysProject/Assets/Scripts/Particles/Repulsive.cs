﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repulsive : MonoBehaviour
{
    
    public bool RepulsiveParticle;
    public bool Wall;
    public bool Violent;
    public bool Contaminated;
    public bool Solid;
    public bool Menu;

    public int particles;

    public float RepulsiveForce;
    public float WallForce;
    public float SolidForce;
    public float ViolentForce;
    public float ContaminatedForce;
    public float DecontaminationForce;
    public Rigidbody2D rb;

    public AudioSource CollisionSound;

    void Start()
    {
        
    }

    void Update()
    {
      if(Menu == false)
      {
        particles = GameObject.Find("Molecule").GetComponent<PlayerController>().particles;  
      } 
    }

    void OnCollisionEnter2D(Collision2D other)
    {
    
      if(RepulsiveParticle == true)
      {
       other.rigidbody.AddForce(-other.contacts[0].normal * RepulsiveForce, ForceMode2D.Impulse);

       if (other.gameObject.CompareTag("Player"))
       {
          CollisionSound.Play();
       }
      }

      if(Wall == true)
      {
       other.rigidbody.AddForce(-other.contacts[0].normal * WallForce, ForceMode2D.Impulse);
      }

      if(Solid == true)
      {
       other.rigidbody.AddForce(-other.contacts[0].normal * SolidForce, ForceMode2D.Impulse);
      }

      if(Violent == true)
      {
       other.rigidbody.AddForce(-other.contacts[0].normal * ViolentForce, ForceMode2D.Impulse);  

       if (other.gameObject.CompareTag("Player"))
       {
          CollisionSound.Play();
       }
      }

      if(Contaminated == true)
      {
        if(particles < 10)
        {
          other.rigidbody.AddForce(-other.contacts[0].normal * ContaminatedForce, ForceMode2D.Impulse);
        }
        else if(other.gameObject.CompareTag("Player"))
        {
          other.rigidbody.AddForce(-other.contacts[0].normal * DecontaminationForce, ForceMode2D.Impulse);
        }
        else
        {
          other.rigidbody.AddForce(-other.contacts[0].normal * ContaminatedForce, ForceMode2D.Impulse);
        }

        if(other.gameObject.CompareTag("Player"))
        {
          CollisionSound.Play();
        }
      }
    }
}
