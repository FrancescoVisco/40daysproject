﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScale : MonoBehaviour
{

    public float ScaleMin;
    public float ScaleMax;
    private float Scale;
    private Vector3 scaleChange;

    void Start()
    {
    }


    void Awake()
    {
        Scale = Random.Range (ScaleMin, ScaleMax);
        this.transform.localScale = new Vector3(Scale, Scale, 1f);
    }

    void Update()
    {
    }
}
