﻿

//Old Sonar Script


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowSonar : MonoBehaviour
{
    public Transform Target;

    void Start()
    {
      Target = GameObject.FindWithTag("Contaminated").transform;
    } 

    void Update()
    {
      Target = GameObject.FindWithTag("Contaminated").transform;
      var dir = Target.position - transform.position;
      var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
      transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
