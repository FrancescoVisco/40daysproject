﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowSonarArrow : MonoBehaviour {

    [SerializeField] public Camera uiCamera;
    [SerializeField] public Sprite arrowSprite;
    [SerializeField] public Sprite noArrow;

    private Vector3 targetPosition;
    private RectTransform pointerRectTransform;
    private Image pointerImage;

    [Header("Basic Sonar settings")]
    public float borderSize;
    public float detectionSize;
    public GameObject SonarArrow;
    public bool SonarIsActivated;
    public Vector3 Contaminated;
    public bool isOffScreen;

    [Header("Spawn/Despawn Sonar settings")]
    public Animator SonarSpawn;
    public float Delay;

    private void Start() 
    {
        pointerRectTransform = transform.Find("SonarArrow").GetComponent<RectTransform>();
        pointerImage = transform.Find("SonarArrow").GetComponent<Image>();
        Contaminated = GameObject.FindWithTag("Contaminated").transform.position;
        SonarIsActivated = GameObject.FindWithTag("Player").GetComponent<PlayerController>().SonarActivated;
    }

    private void Update() 
    {
        Vector3 targetPositionScreenPoint = Camera.main.WorldToScreenPoint(targetPosition);
        isOffScreen = targetPositionScreenPoint.x <= detectionSize || targetPositionScreenPoint.x >= Screen.width - detectionSize || targetPositionScreenPoint.y <= detectionSize || targetPositionScreenPoint.y >= Screen.height - detectionSize;
        Contaminated = GameObject.FindWithTag("Contaminated").transform.position;
        SonarIsActivated = GameObject.FindWithTag("Player").GetComponent<PlayerController>().SonarActivated;

        
        if(SonarIsActivated == true)
        {
           SonarSpawn = SonarArrow.GetComponent<Animator>();
           Show(Contaminated);
           SonarSpawn.SetBool("Spawn", true);
           StopCoroutine("Wait");
        }
        else if(SonarArrow.activeSelf)
        {  
           SonarSpawn.SetBool("Spawn", false);
           StartCoroutine("Wait");
        }

        if (isOffScreen) 
        {
            RotatePointerTowardsTargetPosition();
            pointerImage.sprite = arrowSprite;
            Vector3 cappedTargetScreenPosition = targetPositionScreenPoint;
            if (cappedTargetScreenPosition.x <= borderSize) cappedTargetScreenPosition.x = borderSize;
            if (cappedTargetScreenPosition.x >= Screen.width - borderSize) cappedTargetScreenPosition.x = Screen.width - borderSize;
            if (cappedTargetScreenPosition.y <= borderSize) cappedTargetScreenPosition.y = borderSize;
            if (cappedTargetScreenPosition.y >= Screen.height - borderSize) cappedTargetScreenPosition.y = Screen.height - borderSize;

            Vector3 pointerWorldPosition = uiCamera.ScreenToWorldPoint(cappedTargetScreenPosition);
            pointerRectTransform.position = pointerWorldPosition;
            pointerRectTransform.localPosition = new Vector3(pointerRectTransform.localPosition.x, pointerRectTransform.localPosition.y, 0f);
        }else 
        {
            pointerImage.sprite = noArrow;
            Vector3 pointerWorldPosition = uiCamera.ScreenToWorldPoint(targetPositionScreenPoint);
            pointerRectTransform.position = pointerWorldPosition;
            pointerRectTransform.localPosition = new Vector3(pointerRectTransform.localPosition.x, pointerRectTransform.localPosition.y, 0f);
            pointerRectTransform.localEulerAngles = Vector3.zero;
        }
    }

    private void RotatePointerTowardsTargetPosition() {
        Vector3 toPosition = targetPosition;
        Vector3 fromPosition = Camera.main.transform.position;
        fromPosition.z = 0f;
        Vector3 dir = -(toPosition - fromPosition).normalized;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (angle < 0) angle += 360;
        pointerRectTransform.localEulerAngles = new Vector3(0, 0, angle);
    }

    public void Hide() 
    {
        SonarArrow.SetActive(false);
    }

    public void Show(Vector3 targetPosition) 
    {
        SonarArrow.SetActive(true);
        this.targetPosition = targetPosition;
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(Delay);
        Hide();
        
    }
}
