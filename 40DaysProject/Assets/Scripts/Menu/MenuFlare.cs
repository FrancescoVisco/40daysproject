﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuFlare : MonoBehaviour
{
    public float x;
    public float y;
    public bool OnTrigger;

    void Start()
    {

    }

    void Update()
    {
        //Scale Flare
        y = x;
        transform.localScale = new Vector2(x, y);        

       if(GameObject.Find("ExitTrigger").GetComponent<MainMenu>().OnTrigger == true || GameObject.Find("PlayTrigger").GetComponent<MainMenu>().OnTrigger == true)
       {
           x += Time.deltaTime * 4;
           OnTrigger = true;
       }

       if(GameObject.Find("ExitTrigger").GetComponent<MainMenu>().OnTrigger == false && GameObject.Find("PlayTrigger").GetComponent<MainMenu>().OnTrigger == false)
       {
           x -= Time.deltaTime * 4;
           OnTrigger = false;
       }

       if(x > 6f)
       {
          x = 6f;
       }

       if(x < 0)
       {
          x = 0f;
       }               
    }
}
