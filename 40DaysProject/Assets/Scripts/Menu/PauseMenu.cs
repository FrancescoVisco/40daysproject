﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public bool GameIsPaused = false;
    private int currentSceneIndex;
    public GameObject PauseMenuUI;

    private Scene activeScene;

    private void Start()
    {
        activeScene = SceneManager.GetActiveScene();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {            
                Pause();
            }
        }

        if(GameIsPaused == true)
        {
           Cursor.visible = true;         
        }
        else
        {
           Cursor.visible = false; 
        }

        if(Input.GetKeyDown(KeyCode.Q) && GameIsPaused == true)
        {
           ExitToMenu();
        }

        /*if(Input.GetKeyDown(KeyCode.R))
        {
           Resume();
        }*/
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
}
