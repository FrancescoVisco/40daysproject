﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMolecule : MonoBehaviour
{
	[Header("Basic settings")]
    public float Speed;
	public Rigidbody2D rb;
    public bool Control;

    [Header("Particles settings")]
	public ParticleSystem ps;
	ParticleSystem.EmissionModule emission;
	public float EmissionRateMin;
	public float EmissionRateMax;

	public AudioSource CollisionSound; 

	void Start()
	{
      emission = ps.emission;
	  Control = true;
	}

	void Update()
	{
    
    // Movement key
    	if(Control == true)
		{
       		if((Input.GetKey(KeyCode.RightArrow)) || (Input.GetKey(KeyCode.D)))
	   		{
		   		rb.AddForce(new Vector2 (Speed,0f));
	   		}
	   		if((Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A)))
	   		{
			   	rb.AddForce(new Vector2 (-Speed,0f));
	   		}
	   		if((Input.GetKey(KeyCode.UpArrow)) || (Input.GetKey(KeyCode.W)))
	   		{
			   	rb.AddForce(new Vector2 (0f,Speed));
	   		}
	   		if((Input.GetKey(KeyCode.DownArrow)) || (Input.GetKey(KeyCode.S)))
	   		{
		 	  	rb.AddForce(new Vector2 (0f,-Speed));
	   		}
		}


       // Emission rate Controller
	   if((Input.GetAxis("Horizontal") == 0) && (Input.GetAxis("Vertical") == 0))
	   {
         emission.rateOverTime = EmissionRateMin;
	   }
	   else
	   {
		 emission.rateOverTime = EmissionRateMax;
	   }
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		CollisionSound.Play();
	}
}
