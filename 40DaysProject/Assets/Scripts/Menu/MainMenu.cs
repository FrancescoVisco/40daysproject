﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public bool PlayOn;
    public bool ExitOn;
    public bool OnTrigger;
 
    void Update()
    {
        Cursor.visible = false;
    }

    public void Play()
    {
        if(GameObject.Find("MenuFlare").GetComponent<MenuFlare>().x == 6f)
        {
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
            GameObject.Find("MenuMolecule").GetComponent<MenuMolecule>().Control = false;
        }
    }

    public void Exit()
    {
        if(GameObject.Find("MenuFlare").GetComponent<MenuFlare>().x == 6f)
        {
        Application.Quit();
        Debug.Log("Quit");
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
           if(PlayOn == true)
           {
               OnTrigger = true;
               Play();
           }

           if(ExitOn == true)
           {
               OnTrigger = true;
               Exit();
           }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
           if(PlayOn == true)
           {
               OnTrigger = false;
           }

           if(ExitOn == true)
           {
               OnTrigger = false;
           }
    }
}
